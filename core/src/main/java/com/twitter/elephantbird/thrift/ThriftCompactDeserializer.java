package com.twitter.elephantbird.thrift;

import org.apache.thrift.TBase;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TMemoryInputTransport;

/**
 * DMN
 *
 * Extends TDeserializer to improve handling of corrupt records in a few ways:
 *
 * <ul>
 * <li> sets read-limit for TCompactProtocol before each deserialization.
 *      Reduces OutOfMemoryError exceptions.
 *
 * <li> {@link com.twitter.elephantbird.thrift.ThriftBinaryProtocol} to avoid excessive cpu consumed while
 *      skipping some corrupt records.
 *
 * <li> {@code deserialize(buf, offset, len)} method can avoid buffer copies.
 *      Serialized struct need not span a entire byte array.
 * </ul>
 */
public class ThriftCompactDeserializer extends TDeserializer {

  // use protocol and transport directly instead of using ones in TDeserializer
  private final TMemoryInputTransport trans = new TMemoryInputTransport();

  private final TCompactProtocol protocol = new TCompactProtocol(trans);

  //
  // Generic Wrapper
  //
  private final ThriftProtocolWrapper wrapper = new ThriftProtocolWrapper(protocol);


  public ThriftCompactDeserializer() {
    super(new TCompactProtocol.Factory());
  }

  @Override
  public void deserialize(TBase base, byte[] bytes) throws TException {
    deserialize(base, bytes, 0, bytes.length);
  }

  /**
   * Same as {@link #deserialize(org.apache.thrift.TBase, byte[])}, but much more buffer copy friendly.
   */
  public void deserialize(TBase base, byte[] bytes, int offset, int len) throws TException {
    protocol.reset();
    trans.reset(bytes, offset, len);
    base.read(protocol);
  }
}
